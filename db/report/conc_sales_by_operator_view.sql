CREATE  VIEW conc_sales_by_operator_view  AS
 SELECT sd.id AS id,
    concession_product.name AS Product,
    sd.product_quantity AS QTY,
    sd.unit_selling_amount AS UnitPrice,
    (sd.product_quantity * sd.unit_selling_amount) AS Gross,
    sd.created_by AS Oparetor,
    sd.created_at AS CreateDate

 FROM sells_details AS sd join concession_product on concession_product.id = sd.concession_product_id

