-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 27, 2017 at 01:52 PM
-- Server version: 5.6.33-0ubuntu0.14.04.1
-- PHP Version: 5.6.23-1+deprecated+dontuse+deb.sury.org~trusty+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cinepos`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `sex` enum('Male','Female','Other') DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Admin Table to store admin informations' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `auth_credential`
--

CREATE TABLE IF NOT EXISTS `auth_credential` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `is_admin` tinyint(1) NOT NULL,
  `user_inf_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `username` varchar(500) NOT NULL,
  `password` varchar(500) NOT NULL,
  `activate` tinyint(1) NOT NULL,
  `email_confirmed` tinyint(1) NOT NULL DEFAULT '1',
  `changed_defult_password` tinyint(1) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `app_credential_unser_inf_id` (`user_inf_id`) USING BTREE,
  KEY `role_id` (`role_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE IF NOT EXISTS `booking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `ticket_id` int(11) DEFAULT NULL,
  `seat_id` int(11) DEFAULT NULL,
  `film_time_id` int(11) DEFAULT NULL,
  `booking_amount` decimal(8,2) NOT NULL,
  `status` enum('0','1') NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Stand-in structure for view `BOX_OFFICE_FILM_VIEW`
--
CREATE TABLE IF NOT EXISTS `BOX_OFFICE_FILM_VIEW` (
`id` int(11)
,`name` varchar(255)
,`distributor_id` int(11)
,`rating` float
,`duration_hour` int(11)
,`duration_min` int(11)
,`status` tinyint(1)
,`start_date` date
,`end_date` date
,`is_price_shift` tinyint(1)
,`created_by` int(11)
,`created_at` timestamp
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `BOX_OFFICE_SCHEDULING_VIEW`
--
CREATE TABLE IF NOT EXISTS `BOX_OFFICE_SCHEDULING_VIEW` (
`id` int(11)
,`film_id` int(11)
,`schedule_id` int(11)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `BOX_OFFICE_TIME_VIEW`
--
CREATE TABLE IF NOT EXISTS `BOX_OFFICE_TIME_VIEW` (
`id` int(11)
,`film_schedule_id` int(11)
,`film_id` int(11)
,`start_time` time
,`end_time` time
,`status` tinyint(1)
,`created_by` int(11)
,`created_at` timestamp
);
-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `is_subcategory` tinyint(1) NOT NULL DEFAULT '0',
  `sorted_order` int(11) NOT NULL,
  `picture` text,
  `product_count` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `category_app_login_credential` (`created_by`),
  KEY `category_category` (`parent_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

-- --------------------------------------------------------

--
-- Table structure for table `circuit`
--

CREATE TABLE IF NOT EXISTS `circuit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `site_code` varchar(60) DEFAULT NULL,
  `site_name` varchar(60) NOT NULL,
  `address` varchar(255) NOT NULL,
  `city` varchar(60) NOT NULL,
  `country` varchar(60) NOT NULL,
  `website` varchar(150) NOT NULL,
  `phone_no` varchar(20) NOT NULL,
  `screen_no` int(11) NOT NULL,
  `booking_cancellation_time` time NOT NULL,
  `refund_deduction_percentage` decimal(8,2) NOT NULL,
  `refund_cancellation_time` time NOT NULL,
  `created_by` int(11) DEFAULT '0',
  `updated_by` int(11) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Table structure for table `combo`
--

CREATE TABLE IF NOT EXISTS `combo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `combo_name` varchar(150) NOT NULL,
  `details` varchar(255) NOT NULL,
  `type` enum('TICKET_PRODUCT','PRODUCT') NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `price` decimal(8,2) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

-- --------------------------------------------------------

--
-- Table structure for table `combo_details`
--

CREATE TABLE IF NOT EXISTS `combo_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `combo_id` int(11) DEFAULT NULL,
  `type` varchar(25) NOT NULL,
  `concession_product_id` int(11) DEFAULT NULL,
  `product_quantity` int(3) NOT NULL DEFAULT '0',
  `ticket_quantity` int(3) NOT NULL DEFAULT '0',
  `ticket_id` int(11) DEFAULT NULL,
  `seat_type_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `combo_id` (`combo_id`),
  KEY `ticket_id` (`ticket_id`),
  KEY `concession_product_id` (`concession_product_id`),
  KEY `seat_type_id` (`seat_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

-- --------------------------------------------------------

--
-- Table structure for table `concession_price_shift`
--

CREATE TABLE IF NOT EXISTS `concession_price_shift` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `concession_product_id` int(11) DEFAULT NULL,
  `price` double(8,2) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `concession_product_id` (`concession_product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `concession_product`
--

CREATE TABLE IF NOT EXISTS `concession_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `annotation` varchar(255) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `unit` int(11) NOT NULL,
  `remote_print` tinyint(4) NOT NULL,
  `is_combo` tinyint(4) NOT NULL,
  `status` int(11) NOT NULL,
  `selling_price` decimal(8,2) NOT NULL,
  `buying_price` decimal(8,2) NOT NULL,
  `shifted_price` decimal(8,2) NOT NULL DEFAULT '0.00',
  `is_price_shift` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Table structure for table `concession_product_category`
--

CREATE TABLE IF NOT EXISTS `concession_product_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_by` int(11) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

-- --------------------------------------------------------

--
-- Table structure for table `concession_product_channel`
--

CREATE TABLE IF NOT EXISTS `concession_product_channel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `concession_product_id` int(11) DEFAULT NULL,
  `channel_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `concession_product_image`
--

CREATE TABLE IF NOT EXISTS `concession_product_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `concession_product_id` int(11) DEFAULT NULL,
  `file_path` text NOT NULL,
  `is_banner` tinyint(1) NOT NULL,
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `concession_product_id` (`concession_product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

-- --------------------------------------------------------

--
-- Table structure for table `concession_product_selling`
--

CREATE TABLE IF NOT EXISTS `concession_product_selling` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sell_id` int(11) DEFAULT NULL,
  `selling_amount` decimal(8,2) NOT NULL,
  `quantity` int(11) NOT NULL,
  `concession_product_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `concession_price_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Stand-in structure for view `conc_sales_by_operator_view`
--
CREATE TABLE IF NOT EXISTS `conc_sales_by_operator_view` (
`id` int(11)
,`Product` varchar(255)
,`QTY` int(11)
,`UnitPrice` decimal(8,2)
,`Gross` decimal(18,2)
,`Oparetor` int(11)
,`CreateDate` timestamp
);
-- --------------------------------------------------------

--
-- Table structure for table `distributors`
--

CREATE TABLE IF NOT EXISTS `distributors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `primary_email` varchar(200) NOT NULL,
  `secondary_email` varchar(200) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `address` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

-- --------------------------------------------------------

--
-- Table structure for table `film`
--

CREATE TABLE IF NOT EXISTS `film` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `distributor_id` int(11) DEFAULT NULL,
  `rating` float NOT NULL,
  `duration_hour` int(11) NOT NULL,
  `duration_min` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `is_price_shift` tinyint(1) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `distributor_id` (`distributor_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

-- --------------------------------------------------------

--
-- Table structure for table `film_genre`
--

CREATE TABLE IF NOT EXISTS `film_genre` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `film_id` int(11) DEFAULT NULL,
  `genre_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `film_id` (`film_id`),
  KEY `genre_id` (`genre_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=51 ;

-- --------------------------------------------------------

--
-- Table structure for table `film_image`
--

CREATE TABLE IF NOT EXISTS `film_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `film_id` int(11) DEFAULT NULL,
  `file_path` text NOT NULL,
  `is_banner` tinyint(1) NOT NULL,
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `film_id` (`film_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=43 ;

-- --------------------------------------------------------

--
-- Table structure for table `film_schedule`
--

CREATE TABLE IF NOT EXISTS `film_schedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `screen_id` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `date` date NOT NULL,
  `week_day` int(1) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `screen_id` (`screen_id`),
  KEY `created_at` (`created_at`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=50 ;

-- --------------------------------------------------------

--
-- Table structure for table `film_screen_type`
--

CREATE TABLE IF NOT EXISTS `film_screen_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `film_id` int(11) NOT NULL,
  `screen_dimension_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `film_id` (`film_id`),
  KEY `screen_dimension_id` (`screen_dimension_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=57 ;

-- --------------------------------------------------------

--
-- Table structure for table `film_time`
--

CREATE TABLE IF NOT EXISTS `film_time` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `film_schedule_id` int(11) DEFAULT NULL,
  `film_id` int(11) DEFAULT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `film_schedule_id` (`film_schedule_id`),
  KEY `film_id` (`film_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

-- --------------------------------------------------------

--
-- Table structure for table `film_trailer`
--

CREATE TABLE IF NOT EXISTS `film_trailer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `film_id` int(11) NOT NULL,
  `trailer_url` varchar(500) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `film_id` (`film_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

-- --------------------------------------------------------

--
-- Table structure for table `finance`
--

CREATE TABLE IF NOT EXISTS `finance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `terminal_id` int(11) DEFAULT NULL,
  `update_by` int(11) DEFAULT NULL,
  `calculated_amount` decimal(8,2) NOT NULL,
  `collected_amount` decimal(8,2) NOT NULL,
  `date` date NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `genre`
--

CREATE TABLE IF NOT EXISTS `genre` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

-- --------------------------------------------------------

--
-- Table structure for table `head_office`
--

CREATE TABLE IF NOT EXISTS `head_office` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `circuit_code` varchar(100) NOT NULL,
  `site_name` varchar(50) NOT NULL,
  `owner` varchar(255) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `ip` varchar(10) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Stand-in structure for view `product_summary_view`
--
CREATE TABLE IF NOT EXISTS `product_summary_view` (
`id` int(11)
,`product_name` varchar(255)
,`qty` int(11)
,`selling_price` decimal(8,2)
,`total` decimal(18,2)
,`created_at` timestamp
);
-- --------------------------------------------------------

--
-- Table structure for table `rental_percentage`
--

CREATE TABLE IF NOT EXISTS `rental_percentage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `week_name` varchar(30) NOT NULL,
  `week_percantage` int(3) NOT NULL,
  `film_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `screen`
--

CREATE TABLE IF NOT EXISTS `screen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `is_seat_plan_complete` tinyint(1) NOT NULL,
  `no_of_seat` int(11) NOT NULL,
  `screen_dimension_id` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `row_count` int(11) NOT NULL,
  `column_count` int(11) NOT NULL,
  `opening_time` time NOT NULL,
  `closing_time` time NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Table structure for table `screen_dimension`
--

CREATE TABLE IF NOT EXISTS `screen_dimension` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `screen_seat`
--

CREATE TABLE IF NOT EXISTS `screen_seat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `screen_id` int(11) DEFAULT NULL,
  `seat_type_id` int(11) DEFAULT NULL,
  `name` varchar(150) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `screen_id` (`screen_id`),
  KEY `seat_type_id` (`seat_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

-- --------------------------------------------------------

--
-- Table structure for table `seat_price_shift`
--

CREATE TABLE IF NOT EXISTS `seat_price_shift` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seat_type_id` int(11) DEFAULT NULL,
  `price` double(8,2) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `seat_type_id` (`seat_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Table structure for table `seat_related_price`
--

CREATE TABLE IF NOT EXISTS `seat_related_price` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `price` decimal(8,2) NOT NULL,
  `ticket_id` int(11) DEFAULT NULL,
  `status` enum('0','1') NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `seat_type`
--

CREATE TABLE IF NOT EXISTS `seat_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `is_default` tinyint(1) NOT NULL,
  `adult_price` double NOT NULL,
  `child_price` double NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

-- --------------------------------------------------------

--
-- Table structure for table `sells`
--

CREATE TABLE IF NOT EXISTS `sells` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `screen_id` int(11) DEFAULT NULL,
  `selling_amount` decimal(8,2) NOT NULL,
  `selling_comment` varchar(255) DEFAULT NULL,
  `is_combo` tinyint(4) DEFAULT NULL,
  `product_quantity` int(11) DEFAULT NULL,
  `ticket_quantity` int(11) NOT NULL DEFAULT '0',
  `terminal_id` int(11) DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`),
  KEY `screen_id` (`screen_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

-- --------------------------------------------------------

--
-- Table structure for table `sells_channel`
--

CREATE TABLE IF NOT EXISTS `sells_channel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `sells_details`
--

CREATE TABLE IF NOT EXISTS `sells_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sell_id` int(11) DEFAULT NULL,
  `concession_product_id` int(11) DEFAULT NULL,
  `combo_id` int(11) DEFAULT NULL,
  `ticket_id` bigint(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `unit_selling_amount` decimal(8,2) NOT NULL,
  `product_quantity` int(11) NOT NULL,
  `ticket_quantity` int(11) NOT NULL DEFAULT '0',
  `selling_type` enum('PRODUCT','TICKET','COMBO') NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `sell_id` (`sell_id`),
  KEY `concession_product_id` (`concession_product_id`),
  KEY `combo_id` (`combo_id`),
  KEY `user_id` (`user_id`),
  KEY `ticket_id` (`ticket_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `sells_returns`
--

CREATE TABLE IF NOT EXISTS `sells_returns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sell_id` int(11) DEFAULT NULL,
  `return_amount` decimal(8,2) NOT NULL,
  `selling_amount` decimal(8,2) NOT NULL,
  `refunded_by` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `system_log`
--

CREATE TABLE IF NOT EXISTS `system_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action` varchar(100) NOT NULL,
  `operation` varchar(150) NOT NULL,
  `action_time` time NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `temp_file`
--

CREATE TABLE IF NOT EXISTS `temp_file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` int(11) NOT NULL,
  `path` text NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

-- --------------------------------------------------------

--
-- Table structure for table `terminal`
--

CREATE TABLE IF NOT EXISTS `terminal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `ip_address` varchar(100) NOT NULL,
  `terminal_code` varchar(11) DEFAULT NULL,
  `type` enum('pos','kiosk') NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_by` int(11) DEFAULT '0',
  `updated_by` int(11) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

-- --------------------------------------------------------

--
-- Table structure for table `ticket`
--

CREATE TABLE IF NOT EXISTS `ticket` (
  `id` bigint(15) NOT NULL AUTO_INCREMENT,
  `screen_seat_id` int(11) DEFAULT NULL,
  `film_time_id` int(11) DEFAULT NULL,
  `description` text,
  `annotation` varchar(100) DEFAULT NULL,
  `printed_price` decimal(8,2) NOT NULL,
  `sell_on_web` tinyint(1) NOT NULL,
  `sell_on_pos` tinyint(1) NOT NULL,
  `sell_on_kiosk` tinyint(1) NOT NULL,
  `vat_id` int(11) DEFAULT NULL,
  `is_child` tinyint(1) NOT NULL,
  `is_adult` tinyint(1) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `current_state` enum('AVAILABLE','BOOKED','SOLD') NOT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `seat_type_id` (`screen_seat_id`),
  KEY `film_schedule_id` (`film_time_id`),
  KEY `screen_seat_id` (`screen_seat_id`),
  KEY `film_time_id` (`film_time_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `ticket_channels`
--

CREATE TABLE IF NOT EXISTS `ticket_channels` (
  `id` bigint(15) NOT NULL AUTO_INCREMENT,
  `ticket_id` bigint(15) DEFAULT NULL,
  `channel_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ticket_id` (`ticket_id`),
  KEY `ticket_id_2` (`ticket_id`),
  KEY `ticket_id_3` (`ticket_id`),
  KEY `channel_id_2` (`channel_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `ticket_sells`
--

CREATE TABLE IF NOT EXISTS `ticket_sells` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sell_id` int(11) DEFAULT NULL,
  `ticket_id` int(11) DEFAULT NULL,
  `seat_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `film_id` int(11) DEFAULT NULL,
  `status` enum('0','1') NOT NULL,
  `ticket_price_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_inf`
--

CREATE TABLE IF NOT EXISTS `user_inf` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `email` varchar(150) NOT NULL,
  `dob` date NOT NULL,
  `phone` varchar(20) NOT NULL,
  `address` text NOT NULL,
  `gender` enum('Male','Female','Other') NOT NULL,
  `status` enum('0','1') NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE IF NOT EXISTS `user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(60) NOT NULL,
  `display_name` varchar(100) NOT NULL,
  `group` enum('ADMINSTRATIVE','SALES_USER','APP_USER') NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Table structure for table `vat_settings`
--

CREATE TABLE IF NOT EXISTS `vat_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `amount` decimal(8,2) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Structure for view `BOX_OFFICE_FILM_VIEW`
--
DROP TABLE IF EXISTS `BOX_OFFICE_FILM_VIEW`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `BOX_OFFICE_FILM_VIEW` AS select `film`.`id` AS `id`,`film`.`name` AS `name`,`film`.`distributor_id` AS `distributor_id`,`film`.`rating` AS `rating`,`film`.`duration_hour` AS `duration_hour`,`film`.`duration_min` AS `duration_min`,`film`.`status` AS `status`,`film`.`start_date` AS `start_date`,`film`.`end_date` AS `end_date`,`film`.`is_price_shift` AS `is_price_shift`,`film`.`created_by` AS `created_by`,`film`.`created_at` AS `created_at` from `film`;

-- --------------------------------------------------------

--
-- Structure for view `BOX_OFFICE_SCHEDULING_VIEW`
--
DROP TABLE IF EXISTS `BOX_OFFICE_SCHEDULING_VIEW`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `BOX_OFFICE_SCHEDULING_VIEW` AS select distinct `f`.`id` AS `id`,`f`.`id` AS `film_id`,`ft`.`film_schedule_id` AS `schedule_id` from (`film` `f` join `film_time` `ft` on((`ft`.`film_id` = `f`.`id`))) order by `ft`.`film_schedule_id` desc;

-- --------------------------------------------------------

--
-- Structure for view `BOX_OFFICE_TIME_VIEW`
--
DROP TABLE IF EXISTS `BOX_OFFICE_TIME_VIEW`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `BOX_OFFICE_TIME_VIEW` AS select `film_time`.`id` AS `id`,`film_time`.`film_schedule_id` AS `film_schedule_id`,`film_time`.`film_id` AS `film_id`,`film_time`.`start_time` AS `start_time`,`film_time`.`end_time` AS `end_time`,`film_time`.`status` AS `status`,`film_time`.`created_by` AS `created_by`,`film_time`.`created_at` AS `created_at` from `film_time`;

-- --------------------------------------------------------

--
-- Structure for view `conc_sales_by_operator_view`
--
DROP TABLE IF EXISTS `conc_sales_by_operator_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `conc_sales_by_operator_view` AS select `sd`.`id` AS `id`,`concession_product`.`name` AS `Product`,`sd`.`product_quantity` AS `QTY`,`sd`.`unit_selling_amount` AS `UnitPrice`,(`sd`.`product_quantity` * `sd`.`unit_selling_amount`) AS `Gross`,`sd`.`created_by` AS `Oparetor`,`sd`.`created_at` AS `CreateDate` from (`sells_details` `sd` join `concession_product` on((`concession_product`.`id` = `sd`.`concession_product_id`)));

-- --------------------------------------------------------

--
-- Structure for view `product_summary_view`
--
DROP TABLE IF EXISTS `product_summary_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `product_summary_view` AS select `sd`.`id` AS `id`,`cp`.`name` AS `product_name`,`sd`.`product_quantity` AS `qty`,`sd`.`unit_selling_amount` AS `selling_price`,(`sd`.`product_quantity` * `sd`.`unit_selling_amount`) AS `total`,`sd`.`created_at` AS `created_at` from (`sells_details` `sd` join `concession_product` `cp` on((`sd`.`concession_product_id` = `cp`.`id`))) where (`sd`.`selling_type` = 'product') group by `sd`.`id`;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `auth_credential`
--
ALTER TABLE `auth_credential`
  ADD CONSTRAINT `auth_credential_role` FOREIGN KEY (`role_id`) REFERENCES `user_role` (`id`),
  ADD CONSTRAINT `auth_credential_user_inf` FOREIGN KEY (`user_inf_id`) REFERENCES `user_inf` (`id`);

--
-- Constraints for table `combo_details`
--
ALTER TABLE `combo_details`
  ADD CONSTRAINT `combo_details_combo_id` FOREIGN KEY (`combo_id`) REFERENCES `combo` (`id`),
  ADD CONSTRAINT `combo_details_consession_product` FOREIGN KEY (`concession_product_id`) REFERENCES `concession_product` (`id`),
  ADD CONSTRAINT `combo_details_seat_type_id` FOREIGN KEY (`seat_type_id`) REFERENCES `seat_type` (`id`);

--
-- Constraints for table `concession_price_shift`
--
ALTER TABLE `concession_price_shift`
  ADD CONSTRAINT `concession_price_shift_concession_product` FOREIGN KEY (`concession_product_id`) REFERENCES `concession_product` (`id`);

--
-- Constraints for table `concession_product`
--
ALTER TABLE `concession_product`
  ADD CONSTRAINT `concession_product _category` FOREIGN KEY (`category_id`) REFERENCES `concession_product_category` (`id`);

--
-- Constraints for table `concession_product_image`
--
ALTER TABLE `concession_product_image`
  ADD CONSTRAINT `concession_product_image_consession_product` FOREIGN KEY (`concession_product_id`) REFERENCES `concession_product` (`id`);

--
-- Constraints for table `film`
--
ALTER TABLE `film`
  ADD CONSTRAINT `film_distributor` FOREIGN KEY (`distributor_id`) REFERENCES `distributors` (`id`);

--
-- Constraints for table `film_genre`
--
ALTER TABLE `film_genre`
  ADD CONSTRAINT `film_genre+genre_id` FOREIGN KEY (`genre_id`) REFERENCES `genre` (`id`),
  ADD CONSTRAINT `film_genre_film_id` FOREIGN KEY (`film_id`) REFERENCES `film` (`id`);

--
-- Constraints for table `film_image`
--
ALTER TABLE `film_image`
  ADD CONSTRAINT `film_image_film_id` FOREIGN KEY (`film_id`) REFERENCES `film` (`id`);

--
-- Constraints for table `film_schedule`
--
ALTER TABLE `film_schedule`
  ADD CONSTRAINT `film_schedule_screen_id` FOREIGN KEY (`screen_id`) REFERENCES `screen` (`id`);

--
-- Constraints for table `film_screen_type`
--
ALTER TABLE `film_screen_type`
  ADD CONSTRAINT `film_screen_type_film_id` FOREIGN KEY (`film_id`) REFERENCES `film` (`id`),
  ADD CONSTRAINT `film_screen_type_screen_dimention` FOREIGN KEY (`screen_dimension_id`) REFERENCES `screen_dimension` (`id`);

--
-- Constraints for table `film_time`
--
ALTER TABLE `film_time`
  ADD CONSTRAINT `film_time_film_id` FOREIGN KEY (`film_id`) REFERENCES `film` (`id`),
  ADD CONSTRAINT `film_time_film_schedule_id` FOREIGN KEY (`film_schedule_id`) REFERENCES `film_schedule` (`id`);

--
-- Constraints for table `film_trailer`
--
ALTER TABLE `film_trailer`
  ADD CONSTRAINT `film_trailer_film_id` FOREIGN KEY (`film_id`) REFERENCES `film` (`id`);

--
-- Constraints for table `screen_seat`
--
ALTER TABLE `screen_seat`
  ADD CONSTRAINT `screen_seat_screen_id` FOREIGN KEY (`screen_id`) REFERENCES `screen` (`id`),
  ADD CONSTRAINT `screen_seat_seat_type_id` FOREIGN KEY (`seat_type_id`) REFERENCES `seat_type` (`id`);

--
-- Constraints for table `seat_price_shift`
--
ALTER TABLE `seat_price_shift`
  ADD CONSTRAINT `seat_price_shift` FOREIGN KEY (`seat_type_id`) REFERENCES `seat_type` (`id`);

--
-- Constraints for table `sells`
--
ALTER TABLE `sells`
  ADD CONSTRAINT `sells_created_by` FOREIGN KEY (`created_by`) REFERENCES `auth_credential` (`id`),
  ADD CONSTRAINT `sells_screen_id` FOREIGN KEY (`screen_id`) REFERENCES `screen` (`id`);

--
-- Constraints for table `sells_details`
--
ALTER TABLE `sells_details`
  ADD CONSTRAINT `sells_details_combo` FOREIGN KEY (`combo_id`) REFERENCES `combo` (`id`),
  ADD CONSTRAINT `sells_details_con_product` FOREIGN KEY (`concession_product_id`) REFERENCES `concession_product` (`id`),
  ADD CONSTRAINT `sells_details_sell_id` FOREIGN KEY (`sell_id`) REFERENCES `sells` (`id`),
  ADD CONSTRAINT `sells_details_ticket_id` FOREIGN KEY (`ticket_id`) REFERENCES `ticket` (`id`);

--
-- Constraints for table `ticket`
--
ALTER TABLE `ticket`
  ADD CONSTRAINT `ticket_film_time_id` FOREIGN KEY (`film_time_id`) REFERENCES `film_time` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `ticket_screen_seat_id` FOREIGN KEY (`screen_seat_id`) REFERENCES `screen_seat` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Constraints for table `ticket_channels`
--
ALTER TABLE `ticket_channels`
  ADD CONSTRAINT `ticket_channels_channel_id` FOREIGN KEY (`channel_id`) REFERENCES `sells_channel` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ticket_channels_ticket_id` FOREIGN KEY (`ticket_id`) REFERENCES `ticket` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
