-- phpMyAdmin SQL Dump
-- version 4.6.4deb1+deb.cihar.com~xenial.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 14, 2017 at 01:16 PM
-- Server version: 5.7.19-0ubuntu0.16.04.1
-- PHP Version: 5.6.23-2+deb.sury.org~xenial+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`id`, `role_name`, `display_name`, `group`, `created_by`, `created_at`) VALUES
  (1, 'ADMIN', 'Admin', 'ADMINSTRATIVE', 1, '2017-01-04 06:31:48'),
  (2, 'MANAGER', 'Manager', 'ADMINSTRATIVE', 1, '2017-01-04 06:31:46'),
  (3, 'POS_USER', 'pos user', 'SALES_USER', 1, '2017-02-10 10:22:37'),
  (4, 'APP_USER', 'App user', 'APP_USER', NULL, '2017-02-10 10:22:41');

--
-- Dumping data for table `user_inf`
--

INSERT INTO `user_inf` (`id`, `first_name`, `last_name`, `email`, `dob`, `phone`, `address`, `gender`, `status`, `created_by`, `created_at`) VALUES
  (1, 'System', 'Admin', 'admin@admin.com', '2017-01-10', 'System', 'Nikunja Dhaka', 'Male', '1', 1, '2017-01-04 06:27:00'),
  (2, 'asd', 'asd', 'asdds@asd.com', '2017-01-04', 'asd', 'sad', 'Male', '1', 1, '2017-01-04 11:21:11'),
  (3, 'Zahangir', 'Alam', 'zahangir@alam.com', '2016-01-28', '0316465487', 'Dhaka', 'Male', '1', 1, '2017-01-04 12:46:41'),
  (5, 'Munshi', 'sd', 'sdf@SDf.con', '2017-01-06', 'sdf', 'sfd\nfghfdgh', 'Male', '1', 1, '2017-01-06 08:00:45'),
  (6, 'Banga', 'Bandhu', 'bb@bd.com', '2017-01-12', '01234567', 'Polton', 'Male', '1', 1, '2017-01-06 12:24:26');

--
-- Database: `cinepos`
--

--
-- Dumping data for table `auth_credential`
--

INSERT INTO `auth_credential` (`id`, `is_admin`, `user_inf_id`, `role_id`, `username`, `password`, `activate`, `email_confirmed`, `changed_defult_password`, `created_by`, `created_at`) VALUES
(1, 1, 1, 1, 'admin', '$2a$10$X/PAirQc9C7jRRU5PqNcv.0RJrXaciTOMe2wzkB/bbYDS86PxXuXS', 1, 0, 0, 1, '2017-01-04 06:27:00'),
(2, 1, 2, 1, 'asd', '$2a$10$/aIZMdjAagt5lCad9neR3uC5EmrKsIVsYEgghwacYGvcFopVQGEba', 0, 0, 0, 1, '2017-01-04 11:21:11'),
(3, 1, 3, 3, 'zahangir', '$2a$10$GwI0XkQrJnZPtI12SzKszeBdFPPLGuPdnt7.vhlUffqOIYSQa/cSG', 0, 0, 0, 1, '2017-01-04 12:46:41'),
(4, 1, 5, 1, 'sdf', '$2a$10$xks9RG.HFZAd/9IHF9w7CuxB22WXx6pIFsNxEqK1pWn6ZYYR5HEU6', 1, 0, 0, 1, '2017-01-06 08:00:45'),
(5, 1, 6, 1, 'tansin', '$2a$10$kk5yaeqxXPRsBHrbbAchHe6qgSWvGCkhTXVyNXbNtXoT9tsT9dUoa', 0, 0, 0, 1, '2017-01-06 12:24:26');

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `parent_id`, `is_subcategory`, `sorted_order`, `picture`, `product_count`, `created_by`, `created_date`) VALUES
(8, 'HOME', NULL, 0, 1, '{"original":{"path":"6508271277834.jpg","type":"","size":{"width":1164,"height":775}},"thumb":[]}', 0, 0, '2016-12-22 07:10:05'),
(9, 'HOME APPLIANCE', NULL, 0, 2, NULL, 0, 0, '2016-08-08 09:27:59'),
(10, 'FURNITURE', NULL, 0, 3, NULL, 0, 0, '2016-08-08 09:27:08'),
(11, 'GAMING & PARTY', NULL, 0, 4, NULL, 0, 0, '2016-08-08 09:27:08'),
(12, 'COOL GADGETS', NULL, 0, 5, '{"original":{"path":"21516728661550.jpg","type":"","size":{"width":680,"height":420}},"thumb":[]}', 0, 0, '2016-08-08 09:27:52'),
(13, 'BLOGS', NULL, 0, 6, '{"original":{"path":"21402397931365.jpg","type":"","size":{"width":1600,"height":1200}},"thumb":[]}', 0, 0, '2016-08-08 09:27:52'),
(14, 'Swedish washing machine', 9, 1, 7, NULL, 0, 0, '2016-08-17 11:35:09'),
(15, 'HOME SUBCAT ', 9, 1, 8, 'null', -1, 0, '2016-11-10 06:15:05'),
(16, 'CTG', 8, 1, 9, 'null', 0, 34, '2016-10-14 09:38:24'),
(17, 'DHK', 8, 1, 10, 'null', 0, 34, '2016-10-14 09:38:42');
